import threading
import numpy
from Lima import Core
from Utils import BasePostProcess

class PilatusMaxCountRateTask(Core.Processlib.SinkTaskBase):

    def __init__(self,container):
        Core.Processlib.SinkTaskBase.__init__(self)
        self._container = container
        self._cond = threading.Cond()
        self._mask = None       # mask is reversed
        self._acquisition_time = 1.

        self.clear()
    
    def process(self,data) :
        if self._mask:
            m = numpy.ma.masked_array(data=data.buffer,
                                      mask=self._mask)
            max_value = m.max()
        else:
            max_value = data.buffer.max()

        with self._cond:
            if self._max_value < max_value:
                self._max_value = max_value

    @property
    def max_countrate(self):
        with self._cond:
            return self._max_value / self._acquisition_time

    @property
    def max_countrate(self):
        with self._cond:
            return self._max_value / self._acquisition_time

    @property
    def acquisition_time(self):
        with self._cond:
            return self._acquisition_time

    @acquisition_time.setter
    def acquisition_time(self,value):
        with self._cond:
            self._acquisition_time = value

    def clear(self) :
        with self._cond:
            self._max_value = 0.

class AcqCallback(Core.SoftCallback):
    def __init__(self,container):
        Core.SoftCallback.__init__(self)
        self._container = container

    def prepare(self) :
        #New acquisition will start
        ctControl = _control_ref()
        acq = ctControl.acquisition()
        self._container._task.acquisition_time = acq.getAcqExpoTime()
        self._container._task.clear()

class PilatusMaxCountRateDeviceServer(BasePostProcess):
    #this task name must be unique in the whole process
    PILATUSMAXCOUNTRATE_TASK_NAME = "PilatusMaxCountRate"

    def __init__(self,cl,name) :
        BasePostProcess.__init__(self,cl,name)
        PilatusMaxCountRate.init_device(self)
    
    def init_device(self):
        self._task = PilatusMaxCountRateTask(self)
        self._acq_cbk = AcqCallback(self)
        self._mgr = None

    def set_state(self,state):
        if(state == PyTango.DevState.OFF) :
	    if(self._mgr) :
		self._mgr = None
		ctControl = _control_ref()
		extOpt = ctControl.externalOperation()
		extOpt.delOp(self.PILATUSMAXCOUNTRATE_TASK_NAME)
                self._task.clear()
	elif(state == PyTango.DevState.ON) :
	    if not self._mgr:
                ctControl = _control_ref()
                extOpt = ctControl.externalOperation()
                self._mgr = extOpt.addOp(Core.USER_SINK_TASK,self.PILATUSMAXCOUNTRATE_TASK_NAME,
                                         self._runLevel)
                self._mgr.setSinkTask(self._task)
                self._mgr.registerCallback(self._acq_cbk)
            
        PyTango.Device_4Impl.set_state(self,state)

    def read_max_countrate(self, attr):
        attr.set_value(self._task.max_countrate)

class PilatusMaxCountRateDeviceServerClass(PyTango.DeviceClass):
    #	 Class Properties
    class_property_list = {
	}


    #	 Device Properties
    device_property_list = {
	}


    #	 Command definitions
    cmd_list = {
	'Start':
	[[PyTango.DevVoid,""],
	 [PyTango.DevVoid,""]],
	'Stop':
	[[PyTango.DevVoid,""],
	 [PyTango.DevVoid,""]],
    }

    #	 Attribute definitions
    attr_list = {
	'RunLevel':
	    [[PyTango.DevLong,
	    PyTango.SCALAR,
	    PyTango.READ_WRITE]],
        'max_countrate':
            [[PyTango.DevFloat,
              PyTango.SCALAR,
              PyTango.READ]],
	}

    def __init__(self, name):
	PyTango.DeviceClass.__init__(self, name)
	self.set_type(name);

_control_ref = None
def set_control_ref(control_class_ref) :
    global _control_ref
    _control_ref= control_class_ref

def get_tango_specific_class_n_device() :
   return PilatusMaxCountRateDeviceServerClass,PilatusMaxCountRateDeviceServer
